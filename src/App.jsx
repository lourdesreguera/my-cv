import "./App.scss";
import Header from "./pages/Header";
import { Route, Link, Routes } from "react-router-dom";
import MainCv from "./pages/MainCv";

function App() {
  return (
    <div className="app">
      <Routes>
        <Route path="/" element={<Header />}></Route>
        <Route path="/cv/*" element={<MainCv />}></Route>
      </Routes>
    </div>
  );
}

export default App;
