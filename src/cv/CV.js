const CV = {
    hero: {
      name: "Lourdes",
      lastName: "Reguera",
      email: "louregbri@gmail.com",
      image: "/images/lou2.jpg",
      gitHub: "https://github.com/lourdesreguera",
      aboutMe: "Me gusta estar en constante aprendizaje y evolución, razón por la que decidí dar un giro a mi carrera profesional y dedicarme por completo al mundo del desarrollo web. Me siento cómoda trabajando en equipo, me considero una persona organizada y comprometida con mi trabajo."
    },
    education: [
      {
        name: "Bootcamp FullStack Developer",
        date: "jul 2022 / sept 2022",
        where: "Upgrade Hub",
      },
      {
        name: "Responsive Web Design",
        date: "abr 2022",
        where: "FreeCodeCamp",
      },
      {
        name: "Curso Desarrollo y Diseño Web",
        date: "mar 2022",
        where: "SuperHi",
      },
      {
        name: "Máster en Marketing, Publicidad y Diseño",
        date: "nov 2015 / jun 2016",
        where: "Nett Formación",
      },
      {
        name: "Grado en Administración y Dirección de Empresas",
        date: "sept 2011 / jun 2015",
        where: "Universidad de Sevilla",
      },
    ],
    experience: [
      {
        name: "Administrativo Atención al Usuario",
        date: "mar 2020 / dic 2021",
        where: "Hospital San Rafael (Granada)",
        description:
          "Colaboración. Comunicación. Flexibilidad. Capacidad de trabajo en equipo. Tolerancia a la presión. Empatía.",
      },
      {
        name: "Responsable de Administración",
        date: "oct 2017 / dic 2019",
        where: "Clínica Generalife (Granada)",
        description:
          "Gestión del tiempo. Capacidad de trabajo en equipo. Responsabilidad. Habilidades interpersonales. Resolución de conflictos. Liderazgo.",
      },
      {
        name: "Emprendedora",
        date: "abr 2016 / oct 2018",
        where: "LouB",
        description:
          "Diseño de prendas. Creación de tienda online. Toma de decisiones. Innovación. Iniciativa. Resolución.",
      },
      {
        name: "Hostess y Community Manager",
        date: "dic 2016 / sept 2017",
        where: "Basarri Gin Club (Madrid)",
        description:
          "Creatividad. Habilidades comunicativas. Atención al detalle. Actitud positiva.",
      },
    ],
    languages: [
        "Inglés",
        "Español"
    ],
    skills: [
      "HTML",
      "CSS y SASS",
      "JavaScript",
      "Angular",
      "ReactJS",
      "Node JS",
      "PHP",
      "Symfony",
      "MySQL",
      "Mongo",
      "Git",
      "Photoshop",
      "Figma"
    ],
  };

  export default CV;