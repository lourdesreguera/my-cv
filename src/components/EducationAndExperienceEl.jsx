import React from "react";
import "./EducationAndExperience.scss";

const EducationAndExperienceEl = ({ info }) => {
  return (
        <div className="educationAndExperience__container">
          <div className="educationAndExperience__container--date">
            <p>{info.date}</p>
          </div>
          <div className="educationAndExperience__container--text">
            <p className="educationAndExperience__container--name">
              {info.name}
            </p>
            <p>{info.where}</p>
            {info.description && (
              <p className="educationAndExperience__container--desc">
                {info.description}
              </p>
            )}{" "}
          </div>
        </div>
      );
};

export default EducationAndExperienceEl;
