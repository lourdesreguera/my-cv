import React from "react";
import "./Navbar.scss";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar">
      <nav>
        <NavLink to="education" activeclass={'active'}>Formación</NavLink>
        <NavLink to="experience" activeclass={'active'}>Experiencia</NavLink>
      </nav>
    </div>
  );
};

export default Navbar;
