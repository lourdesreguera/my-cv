import React from "react";
import './About.scss'

const About = ({ hero }) => {
  return (
    <div className="about">
      <h3>About</h3>
      <p>{hero.aboutMe}</p>
    </div>
  );
};

export default About;
