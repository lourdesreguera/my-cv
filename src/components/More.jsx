import React from "react";
import "./More.scss";

const More = ({ languages, skills }) => {
  return (
    <div className="more">
      <div className="more__lang">
        <h3>Idiomas</h3>
        <div>
          {languages.map((language, i) => {
            return <p key={i}>{language}</p>;
          })}
        </div>
      </div>
      <div>
        <h3>Tecnologías</h3>
        <div className="more__skills">
          {skills.map((skill, i) => {
            return <p key={i}>{skill}</p>;
          })}
        </div>
      </div>
    </div>
  );
};

export default More;
