import React from "react";
import "./Hero.scss";

const Hero = ({ hero }) => {
  return (
    <div className="hero">
      <img src={hero.image} />
      <h1>
        {hero.name} {hero.lastName}
      </h1>
      <a href="mailto:{hero.email}" target="_blank" rel="noreferrer">{hero.email}</a>
      <a href={hero.gitHub} target="_blank" rel="noreferrer">{hero.gitHub}</a>
    </div>
  );
};

export default Hero;
