import React from "react";
import "../../src/components/EducationAndExperience.scss";
import EducationAndExperienceEl from "../components/EducationAndExperienceEl"

const Education = ({ info }) => {
    return (
        <div className="educationAndExperience">
          {info.map((ed, i) => {
            return <EducationAndExperienceEl info={ed} key={i} />;
          })}
        </div>
      );
};

export default Education;