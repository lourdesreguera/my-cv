import React from "react";
import CV from "../cv/CV";
import Hero from "../../src/components/Hero";
import Navbar from "../../src/components/Navbar";
import More from "../../src/components/More";
import About from "../../src/components/About";
import { Route, Link, Routes } from "react-router-dom";
import EducationAndExperience from "./EducationAndExperience";
import { useEffect } from "react";

const { hero, education, experience, languages, skills } = CV;

const MainCv = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div>
      <Hero hero={hero} />
      <About hero={hero} />
      <Navbar />
      <Routes>
        <Route
          index
          element={<EducationAndExperience info={education} />}
        ></Route>
        <Route
          path="/education"
          element={<EducationAndExperience info={education} />}
        ></Route>
        <Route
          path="/experience"
          element={<EducationAndExperience info={experience} />}
        ></Route>
      </Routes>
      <More languages={languages} skills={skills} />
    </div>
  );
};

export default MainCv;
