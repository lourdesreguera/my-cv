import React from "react";
import "./Header.scss";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <>
      <section className="header header--first"></section>
      <section className="header header--second"></section>
      <section className="header header--third"></section>
      <section className="header header--fourth"></section>
      <section className="header header--fith">
        <p>¿Quieres saber más sobre mí?</p>
        <Link to="/cv">
          <img src="/images/expand.svg" />
        </Link>
      </section>
    </>
  );
};

export default Header;
